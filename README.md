# Установка

### Конфиг

Прописать в .env (пример в .env.dist) коннект к базе и доступ в яндекс.xml

### Зависимости

Устаносить composer если его нет

    https://getcomposer.org/doc/00-intro.md#installation-windows

Запустить установку зависимостей
    
    composer install
    
# Использование

### Добавление сайтов по словам

    ./bin/console app:add-by-words ./path/to/phrase/list.txt
    
#### Вывести список сайтов

В формате CSV с сохранением в файл

     ./bin/console app:show-site >/path/to/file.csv
     
На экран в виде таблицы

    ./bin/console app:show-site --table