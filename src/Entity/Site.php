<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteRepository")
 */
class Site
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=511)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $scheme;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="date")
     */
    private $registered;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ns_server;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $page_count_yandex;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $page_count_google;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $visitor_per_day;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $visitor_per_month;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getScheme(): ?string
    {
        return $this->scheme;
    }

    public function setScheme(string $scheme): self
    {
        $this->scheme = $scheme;

        return $this;
    }

    public function getDomainWithScheme(): string
    {
        return $this->scheme.'://'.$this->domain;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRegistered(): ?\DateTimeInterface
    {
        return $this->registered;
    }

    public function setRegistered(\DateTimeInterface $registered): self
    {
        $this->registered = $registered;

        return $this;
    }

    public function getNsServer(): ?string
    {
        return $this->ns_server;
    }

    public function setNsServer(?string $ns_server): self
    {
        $this->ns_server = $ns_server;

        return $this;
    }

    public function getPageCountYandex(): ?int
    {
        return $this->page_count_yandex;
    }

    public function setPageCountYandex(?int $page_count_yandex): self
    {
        $this->page_count_yandex = $page_count_yandex;

        return $this;
    }

    public function getPageCountGoogle(): ?int
    {
        return $this->page_count_google;
    }

    public function setPageCountGoogle(?int $page_count_google): self
    {
        $this->page_count_google = $page_count_google;

        return $this;
    }

    public function getVisitorPerDay(): ?int
    {
        return $this->visitor_per_day;
    }

    public function setVisitorPerDay(?int $visitor_per_day): self
    {
        $this->visitor_per_day = $visitor_per_day;

        return $this;
    }

    public function getVisitorPerMonth(): ?int
    {
        return $this->visitor_per_month;
    }

    public function setVisitorPerMonth(?int $visitor_per_month): self
    {
        $this->visitor_per_month = $visitor_per_month;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }
}
