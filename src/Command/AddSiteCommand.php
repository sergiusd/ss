<?php

namespace App\Command;

use AntonShevchuk\YandexXml\YandexXmlClient;
use App\Entity\Site;
use App\Exception\GetContentException;
use App\Repository\SiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AntonShevchuk\YandexXml\Exceptions\YandexXmlException;
use voku\helper\HtmlDomParser;
use Novutec\WhoisParser\Parser;

class AddSiteCommand extends Command
{
    const YA_WAIT_MINUTE = 5; // пауза если закончисла лимит
    const YA_PAUSE_SECOND = 1; // пауза между запросами на яндекс
    const YA_REGION = 1; // https://promoexpert.pro/wp-content/uploads/2018/05/yandex.ru-yaca-geo.c2n.txt
    const PAGE_MAX = 10;
    const PAGE_SIZE = 100; // Количество результатов на странице (макс 100)
    const SITE_INTERVAL_SEC_MIN = 1;
    const SITE_INTERVAL_SEC_MAX = 2;
    const REQUEST_TIMEOUT = 4; // Таймаут на все запросы

    protected static $defaultName = 'app:add-by-words';

    /** @var OutputInterface */
    private $out;

    /** @var EntityManagerInterface */
    private $em;
    /** @var SiteRepository */
    private $siteRepository;

    private $yaXmlKey;
    private $yaXmlSecret;

    public function __construct(
        EntityManagerInterface $em,
        SiteRepository $siteRepository,
        string $yaXmlKey,
        string $yaXmlSecret
    ) {
        $this->em = $em;
        $this->siteRepository = $siteRepository;
        $this->yaXmlKey = $yaXmlKey;
        $this->yaXmlSecret = $yaXmlSecret;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this
            ->addArgument('path', InputArgument::REQUIRED, 'Путь к файлу с фразами')
            ->setDescription('Collect site by words');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;

//        $site = (new Site())->setScheme('http')->setDomain('TheHorses.ru');
//        $this->fillWhois($site);
//        $this->fillHost($site);
//        $this->fillPrCy($site);
//        var_dump($site);
//        die;

        try {
            $phraseList = $this->getPhraseList($input->getArgument('path'));
            foreach ($phraseList as $phrase) {
                $this->out->writeln('');
                $this->out->writeln('<fg=green>##### ' . $phrase . '</>');
                $this->out->writeln('');
                $siteList = $this->parseYandexByWordUseXMLService($phrase);
                foreach ($siteList as $site) {
                    if ($output->isVerbose()) {
                        $this->out->writeln("Process " . $site->getDomain());
                    }
                    // Если домен уже есть то не обрабатываем
                    $this->dbCheckConnect();
                    if (null !== $this->siteRepository->findOneBy(['domain' => $site->getDomain()])) {
                        $this->out->writeln('<comment>Домен ' . $site->getDomain() . ' уже есть</comment>');
                        continue;
                    }
                    sleep(rand(self::SITE_INTERVAL_SEC_MIN, self::SITE_INTERVAL_SEC_MAX));
                    try {
                        $this->fillWhois($site);
                        $this->fillHost($site);
                        $this->fillPrCy($site);
                        $site->setUpdated(new \DateTime());
                        $this->dbCheckConnect();
                        $this->em->persist($site);
                        $this->em->flush();
                        $this->out->writeln('<info>Добавлен домен ' . $site->getDomain() . '</info>');
                    } catch (\RuntimeException $e) {
                        $this->out->writeln(
                            '<comment>Домен ' . $site->getDomain() . ' пропущен, '.
                            'проблема: ' . $e->getMessage() . '</comment>'
                        );
                    }
                }
            }
        } catch (\Exception $e) {
            if ($output->isVerbose()) {
                throw $e;
            } else {
                $this->out->writeln('<error>'.$e->getMessage().'</error>');
            }
            return 1;
        }
    }

    private function getPhraseList(string $phrasePath): array {
        if (!file_exists($phrasePath)) {
            throw new \Exception('Не найден файл '.$phrasePath);
        }
        $data = file($phrasePath);
        return array_filter(array_map(function ($line) {
            return preg_replace('/\s*$/u', '', $line);
        }, $data));
    }
    
    protected function dbCheckConnect() {
        if ($this->em->getConnection()->ping() === false) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }
    }

    protected function getContent(string $uri): string
    {
        $ctx = stream_context_create([
            'http'=> [
                'timeout' => self::REQUEST_TIMEOUT,
            ],
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ]);
        $res =  file_get_contents($uri, false, $ctx);
        if (false === $res) {
            $error = error_get_last();
            throw new GetContentException(
                $error && isset($error['message'])
                    ? (isset($error['type']) ? '['.$error['type'].'] ' : '').$error['message']
                    : 'Unknown error'
            );
        }
        return $res;
    }

    /**
     * @param string $query
     * @return array|Site[]
     * @throws \Exception
     */
    protected function parseYandexByWordUseXMLService(string $query): array {
        $page = 0;
        $ret = [];
        do {
            do {
                $isRepeat = false;
                try {
                    sleep(self::YA_PAUSE_SECOND);
                    $client = new YandexXmlClient($this->yaXmlKey, $this->yaXmlSecret);
                    $response = $client
                        ->query($query)
                        ->lr(self::YA_REGION)
                        ->page($page)
                        ->limit(self::PAGE_SIZE)
                        //->setProxy('host или ip', 'port', 'user, если требуется авторизация', 'pass, если требуется авторизация') // Если требуется проксирование запроса
                        ->request();
                } catch (YandexXmlException $e) {
                    if (32 === $e->getCode()) {
                        $this->out->writeln('Закончился лимит у яндекса, пауза '.self::YA_WAIT_MINUTE.' минут...');
                        sleep(self::YA_WAIT_MINUTE * 60);
                        $isRepeat = true;
                    } else {
                        throw new \Exception(
                            "Возникло исключение YandexXmlException: " . $e->getMessage().
                            (
                                isset($client) && isset($client->response) && isset ($client->response->error)
                                    ? "\nОригинальная ошибка: ".$client->response->error
                                    : ''
                            )
                        );
                    }
                }
            } while ($isRepeat);

            foreach ($response->results() as $i => $result) {
                $site = new Site();
                $site->setScheme(parse_url($result->url, PHP_URL_SCHEME));
                $site->setDomain($result->domain);
                $ret[] = $site;
            }
            $page++;
        } while($page < min(self::PAGE_MAX, $response->pages()));

        return $ret;
    }

    private function fillWhois(Site $site): void
    {
        $parser = new Parser();
        $result = $parser->lookup($site->getDomain());
        $ns = $result->nameserver ?? [];
        $created = $result->created ?? null;
        $site->setNsServer(join(', ', array_map(function($a) {return rtrim($a, '.');}, $ns)));
        if ($created) {
            try {
                $site->setRegistered(new \DateTime($created));
            } catch (\Exception $e) {}
        }
    }

    private function fillHost(Site $site): void
    {
        $url = $site->getDomainWithScheme();
        try {
            $content = $this->getContent($url);
            $dom = HtmlDomParser::str_get_html($content);

            $titleEl = $dom->find('head title');
            $name = '';
            if (count($titleEl)) {
                $name = strval($this->normalizeString($titleEl[0]->innerText));
            }
            $site->setName($name);

            $bodyEl = $dom->find('body');
            if (count($bodyEl)) {
                $body = $bodyEl[0]->innerText;
                if (preg_match('/(?:\+7|8)([\-\s\(]*\d{3}[\)\-\s]*\d{3}[\-\s]*\d{2}[\-\s]*\d{2})/u', $body, $m)) {
                    $site->setPhone('7' . strtr($m[1], [' ' => '', '-' => '', '(' => '', ')' => '']));
                }
            }
        } catch (GetContentException $e) {
            $this->out->writeln(
                '<comment>Не вышло получить контент ' . $url . ', телефон не получен, '.
                'проблема: ' . $e->getMessage() . '</comment>'
            );
        }
    }

    private function normalizeString(string $str): string
    {
        //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
        $str = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
            '|[\x00-\x7F][\x80-\xBF]+'.
            '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
            '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
            '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
            '?', $str );
        //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
        $str = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
            '|\xED[\xA0-\xBF][\x80-\xBF]/S','?', $str );
        return $str;
    }

    private function fillPrCy(Site $site): void
    {
        $url = 'https://a.pr-cy.ru/'.$site->getDomain().'/';
        try {
            $content = $this->getContent($url);
            $dom = HtmlDomParser::str_get_html($content);
            // page count
            $yandexIndexEl = $dom->find('div#yandexIndex a[target=_blank]');
            $googleIndexEl = $dom->find('div#googleIndex a[target=_blank]');
            $site->setPageCountYandex($this->convertPrCyToInt($yandexIndexEl));
            $site->setPageCountGoogle($this->convertPrCyToInt($googleIndexEl));
            // visitors
            $statEl = $dom->find('div#publicStatistics tr:nth-child(2)');
            if (count($statEl)) {
                $perDayEl = $statEl[0]->find('td:nth-child(2)');
                $perMonthEl = $statEl[0]->find('td:nth-child(4)');
                $site->setVisitorPerDay($this->convertPrCyToInt($perDayEl));
                $site->setVisitorPerMonth($this->convertPrCyToInt($perMonthEl));
            }
        } catch (GetContentException $e) {
            $this->out->writeln(
                '<comment>Не вышло получить контент ' . $url . ', статистика не получена, '.
                'проблема: ' . $e->getMessage() . '</comment>'
            );
        }
    }

    private function convertPrCyToInt($elements): ?int
    {
        return count($elements)
            ? intval(preg_replace('/[^\d]/', '', html_entity_decode($elements[0]->innerText)))
            : null;
    }

    private function getCurrentLimit(): ?int
    {
        $data = $this->getContent(sprintf(
            'https://yandex.ru/search/xml?action=limits-info&user=%s&key=%s',
            $this->yaXmlKey,
            $this->yaXmlSecret
        ));
        $xml = new \SimpleXMLElement($data);
        $now = new \DateTime();
        foreach($xml->response[0]->limits[0]->{'time-interval'} as $limit) {
            try {
                $from = new \DateTime($limit['from']);
                $to = new \DateTime($limit['to']);
            } catch (\Exception $e) {
                echo 'SKip '.$limit['from']."\n";
                continue;
            }
            if ($from < $now && $to > $now) {
                return intval($limit);
            }
        }
        return null;
    }
}