<?php

namespace App\Command;

use App\Entity\Site;
use App\Repository\SiteRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ShowSiteCommand extends Command
{
    protected static $defaultName = 'app:show-site';

    const CSV_DELIMITER = ';';

    /** @var SiteRepository */
    private $siteRepository;

    public function __construct(
        SiteRepository $siteRepository
    ) {
        $this->siteRepository = $siteRepository;
        parent::__construct(null);
    }

    protected function configure()
    {
        $this
            ->addOption('table', 't', InputOption::VALUE_NONE, 'Показать в формате таблицы')
            ->setDescription('Show sites');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityList = $this->siteRepository->findBy([], ['id' => 'asc']);

        $headers = [
            'Домен',
            'Телефон с сайта',
            'Дата регистрации',
            'НС сервера',
            'Страницы в Яндексе',
            'Страницы в Google',
            'Трафик в день',
            'Трафик в месяц',
        ];
        $data = array_map(function (Site $s) {
            return [
                $s->getDomain(),
                $s->getPhone(),
                $s->getRegistered() ? $s->getRegistered()->format('d.m.Y') : '',
                $s->getNsServer(),
                $s->getPageCountYandex(),
                $s->getPageCountGoogle(),
                $s->getVisitorPerDay(),
                $s->getVisitorPerMonth(),
            ];
        }, $entityList);

        if ($input->getOption('table')) {
            $io = new SymfonyStyle($input, $output);
            $io->table($headers, $data);
        } else {
            $df = fopen("php://output", 'w');
            fputcsv($df, $headers, self::CSV_DELIMITER);
            foreach ($data as $row) {
                fputcsv($df, $row, self::CSV_DELIMITER);
            }
            fclose($df);
        }
    }
}